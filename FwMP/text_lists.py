import re, time
from functools import partial
import pandas as pd
#from spacy.tokens.doc import Doc as spDoc
#from spacy.tokens.span import Span as spSpan
#from spacy.tokens.token import Token as spTok
from FwUtility import run_parallel, cores_to_use, secondsToStr, process_time, process_memory
import FwFreq
from FwText import (
    word_tokenize,
    title_tokenize, description_tokenize, split_strip,
    all_ngrams_tuples, tuple_ngram_coverage_in_list,
)
from FwText import sentence_tokenize as sent_tokenize
from FwPTypes import mc_ptypes

i_CORES_TO_USE = cores_to_use()
i_MIN_MP_LIST_SIZE = 2000  # it's slower to use set up and close MP instances than to run small lists on single core

re_digit = re.compile(r'[1-9]')


def text_to_ngrams(text, i_max_size=6, i_min_size=1,
                   stop_words="english", remove_punctuation=True,
                   b_case_insensitive=True):
    ''' Pass in a doc (string such as sentences or attribute values). '''
    if isinstance(text, str):
        if b_case_insensitive:
            text = text.lower()
        a_words = word_tokenize(text, remove_punctuation, stop_words=stop_words)
    else:
        a_words = list(text)
    a_ngrams = all_ngrams_tuples(a_words, i_max_size, i_min_size, stop_words)
    return a_ngrams


def text_list_to_ngram_list(a_text_list, **kwargs):
    ''' Pass in a list of docs (strings such as sentences or attribute values).
    '''
    if i_CORES_TO_USE != 1 and len(a_text_list) > i_MIN_MP_LIST_SIZE:  # run MP on large lists
        a_ngram_list = run_parallel(
            a_text_list, text_to_ngrams,
            kwargs,
            n_cores=i_CORES_TO_USE
        )

    else:  # run small lists on single core
        a_ngram_list = [text_to_ngrams(text, **kwargs) for text in a_text_list]
    return a_ngram_list


def tokenize_texts(a_texts, **kwargs):
    f_tokenize = partial(word_tokenize, **kwargs)
    if i_CORES_TO_USE != 1 and len(a_texts) > i_MIN_MP_LIST_SIZE:  # run MP on large lists
        a_tok_texts = run_parallel(
            a_texts,
            f_tokenize,
            n_cores=i_CORES_TO_USE
        )

    else:  # run small lists on single core
        a_tok_texts = [f_tokenize(text) for text in a_texts]
    
    return a_tok_texts


def tokenize_titles(a_titles, **kwargs):
    f_tokenize = partial(title_tokenize, **kwargs)
    if i_CORES_TO_USE != 1 and len(a_titles) > i_MIN_MP_LIST_SIZE:  # run MP on large lists
        a_tok_titles = run_parallel(
            a_titles,
            f_tokenize,
            n_cores=i_CORES_TO_USE
        )

    else:  # run small lists on single core
        a_tok_titles = [f_tokenize(title) for title in a_titles]
    return a_tok_titles


def tokenize_descriptions(a_descriptions, **kwargs):
    f_tokenize = partial(description_tokenize, **kwargs)
    if i_CORES_TO_USE != 1 and len(a_descriptions) > i_MIN_MP_LIST_SIZE:  # run MP on large lists
        a_tok_descs = run_parallel(
            a_descriptions,
            f_tokenize,
            n_cores=i_CORES_TO_USE
        )

    else:  # run small lists on single core
        a_tok_descs = [f_tokenize(desc) for desc in a_descriptions]
                                                 
    return a_tok_descs

    
def sentence_tokenize(a_texts, **kwargs):
    f_tokenize = partial(sent_tokenize, **kwargs)
    if i_CORES_TO_USE != 1 and len(a_texts) > i_MIN_MP_LIST_SIZE:  # run MP on large lists
        a_sentences = run_parallel(
            a_texts,
            f_tokenize,
            n_cores=i_CORES_TO_USE
        )

    else:  # run small lists on single core
        a_sentences = [f_tokenize(sent) for sent in a_texts]
    return a_sentences


def dataset_ngrams(a_titles,
                   a_descriptions=[],
                   a_brands=[],
                   a_categories=[],
                   e_phrase_list={},
                   dictionary=None,
                   trademarks=None,
                   b_normalize=True,  # turn all digits into zeros
                   b_return_percents=True,
                   i_max_size=5,
                   b_return_lower=True,
                   b_dimensionize_titles=True,
                   b_dimensionize_descriptions=False,
                   b_skip_print=False):
    ''' Return a list of ngrams for whole dataset, PNs, descriptions, brands, categories, and counts. '''
    a_dataset_ngrams = []
    e_pn_ngrams, e_desc_ngrams, e_brand_ngrams, e_mc_ngrams = {}, {}, {}, {}
    i_pn_word_count, i_desc_word_count, i_brand_word_count, i_cat_word_count = 0, 0, 0, 0
    
    if isinstance(a_titles, pd.Series):
        a_titles = list(a_titles.fillna('').astype(str))
    if isinstance(a_descriptions, pd.Series):
        a_descriptions = list(a_descriptions.fillna('').astype(str))
    if isinstance(a_brands, pd.Series):
        a_brands = list(a_brands.fillna('').astype(str))
    if isinstance(a_categories, pd.Series):
        a_categories = list(a_categories.fillna('').astype(str))
                
    if a_titles:
        if isinstance(a_titles[0], str):
            n_start = time.time()
            a_titles = tokenize_titles(
                a_titles,
                phrases=e_phrase_list,
                return_lower=b_return_lower,
                dimensionize=b_dimensionize_titles)
        
            if not b_skip_print:
                print('Tokenizing titles:\t{}\t{}\t{}'.format(
                    secondsToStr(time.time() - n_start), process_time(), process_memory()))
            
        # if b_normalize: a_titles = [[re_digit.sub('0',s_word) for s_word in a_title] for a_title in a_titles]
        i_pn_word_count = sum([len(a_pn) for a_pn in a_titles])
        e_pn_ngrams = tuple_ngram_coverage_in_list(
            [all_ngrams_tuples([re_digit.sub('0', word) if b_normalize else word for word in a_pn],
                                      i_max_size=i_max_size)
             for a_pn in a_titles
             if a_pn]
        )
        if b_return_percents:
            e_pn_ngrams = FwFreq.freq_dict_to_percent(e_pn_ngrams, i_total=i_pn_word_count)
        a_dataset_ngrams.append(e_pn_ngrams)
        
        if not b_skip_print:
            print('Title ngrams:\t{}\t{}\t{}'.format(
                secondsToStr(time.time() - n_start), process_time(), process_memory()))
        
    if a_descriptions:
        if isinstance(a_descriptions[0], str) or \
           (isinstance(a_descriptions[0], (list, tuple)) and
           isinstance(a_descriptions[0][0], str)):
            n_start = time.time()
            a_descriptions = tokenize_descriptions(
                a_descriptions,
                phrases=e_phrase_list,
                dictionary=dictionary,
                trademarks=trademarks,
                return_sentences=True,
                return_lower=b_return_lower,
                dimensionize=b_dimensionize_descriptions)
            if not b_skip_print:
                print('Tokenizing descriptions:\t{}\t{}\t{}'.format(
                    secondsToStr(time.time() - n_start), process_time(), process_memory()))
        
        # if b_normalize: a_descriptions = [[[re_digit.sub('0',s_word) for s_word in a_sent]
        #                                    for a_sent in a_desc] for a_desc in a_descriptions]
        i_desc_word_count = sum([len(a_sent) for a_desc in a_descriptions for a_sent in a_desc])
        e_desc_ngrams = tuple_ngram_coverage_in_list(
            [all_ngrams_tuples(desc_text(a_sent, b_normalize, b_lower=True), i_max_size=i_max_size)
             for a_desc in a_descriptions
             for a_sent in a_desc if a_desc])
        if b_return_percents:
            e_desc_ngrams = FwFreq.freq_dict_to_percent(e_desc_ngrams, i_total=i_desc_word_count)
        a_dataset_ngrams.append(e_desc_ngrams)
        
        if not b_skip_print:
            print('Description ngrams:\t{}\t{}\t{}'.format(
                secondsToStr(time.time() - n_start), process_time(), process_memory()))

    if a_brands:
        if isinstance(a_brands[0], str):
            n_start = time.time()
            a_brands = [
                tuple(a_brand) for a_brand in
                tokenize_texts(a_brands,
                return_lower=b_return_lower,
                remove_punct=False,
                stop_words=False)
            ]
            if not b_skip_print:
                print('Tokenizing brands:\t{}\t{}\t{}'.format(
                    secondsToStr(time.time() - n_start), process_time(), process_memory()))
            
        i_brand_word_count = sum([len(t_brand) for t_brand in a_brands])
        e_brand_ngrams = FwFreq.freq([t_brand for t_brand in a_brands if t_brand], remove_empty=True)
        e_brand_ngrams = {tuple(ngram.split()) if isinstance(ngram, str) else ngram: freq
                          for ngram, freq in e_brand_ngrams.items()}  # convert brand_ngrams to tuples
        if b_return_percents:
            e_brand_ngrams = FwFreq.freq_dict_to_percent(e_brand_ngrams, i_total=i_brand_word_count)
        a_dataset_ngrams.append(e_brand_ngrams)
        
        if not b_skip_print:
            print('Brand ngrams:\t{}\t{}\t{}'.format(
                secondsToStr(time.time() - n_start), process_time(), process_memory()))
        
    if a_categories:
        # need to make mc_ptypes.mcpt_ngrams accept a pretokenized mc of nodes and words
        n_start = time.time()
        if b_return_lower:
            a_cat_words = [word_tokenize(s_node, stop_words=None)
                           for mc in a_categories for s_node in split_strip(mc.lower(), '>')]
        else:
            a_cat_words = [word_tokenize(s_node, stop_words=None)
                           for mc in a_categories for s_node in split_strip(mc, '>')]
        if not b_skip_print:
            print('Tokenizing categories:\t{}\t{}\t{}'.format(
                secondsToStr(time.time() - n_start), process_time(), process_memory()))
                       
        n_start = time.time()
        i_cat_word_count = len(FwFreq.flatten(a_cat_words))
        a_mcs = [mc_ptypes.mcpt_ngrams(mc.lower()) for mc in a_categories]
        e_mc_ngrams = FwFreq.freq(a_mcs, remove_empty=True)
        e_mc_ngrams = {tuple(ngram.split()) if isinstance(ngram, str) else ngram: freq
                       for ngram, freq in e_mc_ngrams.items()}  # convert mc_ngrams to tuples
        if b_return_percents:
            e_mc_ngrams = FwFreq.freq_dict_to_percent(e_mc_ngrams, i_total=i_cat_word_count)
        a_dataset_ngrams.append(e_mc_ngrams)
        
        if not b_skip_print:
            print('Category ngrams:\t{}\t{}\t{}'.format(
                secondsToStr(time.time() - n_start), process_time(), process_memory()))
    
    e_df_ngrams = FwFreq.add_freq_percent_dicts(*a_dataset_ngrams, b_average=b_return_percents)
    
    t_counts = (i_pn_word_count + i_desc_word_count + i_brand_word_count + i_cat_word_count,
                i_pn_word_count,
                i_desc_word_count,
                i_brand_word_count,
                i_cat_word_count)
    return e_df_ngrams, e_pn_ngrams, e_desc_ngrams, e_brand_ngrams, e_mc_ngrams, t_counts


def desc_text(a_sent, b_normalize=False, b_lower=False):
    if not a_sent: return []
    if type(a_sent).__name__ == 'Doc' or type(a_sent[0]).__name__ == 'Token':
        a_sent = [word.text for word in a_sent if word.text]
    
    if b_normalize:
        a_sent = [re_digit.sub('0', word) for word in a_sent]
    
    if b_lower:
        a_sent = [word.lower() for word in a_sent]
    return a_sent
