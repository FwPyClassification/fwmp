'''
Test cases for FwMP/text_lists
'''
from __future__ import absolute_import
import math
import pandas as pd
#import spacy
from FwMP import text_lists


df_test = pd.DataFrame()
df_test['titles'] = [
    "Wellness Signature Selects Chicken with Liver Entree Canned Cat Food, (5.3 oz.), Case of 24",
    "NeilMed NasaRinse Naso-Gel , 1 oz"
]
df_test['brands'] = ['Wellness', 'NeilMed']
df_test['descriptions'] = [
    "Wellness Cat Food is specially formulated with high quality protein and natural sources "
    "of essential nutrients that your cat needs. This formula contains premium quality meat, "
    "sweet potatoes and carrots and fresh berries to offer excellent sources of protein, "
    "vitamins, minerals, fatty acids, and amino acids. These nutrients are carefully chosen to "
    "ensure your cat's skin, coat, urinary tract, immune system, eyes and heart are properly "
    "maintained. This can of Wellness Cat Food comes with 5.5 ounces of a palatable chicken and "
    "lobster formula. There is also a Healthy Indulgence formula available for cats with grain "
    "allergies. Your cat will love this scrumptious food, so order a can for your feline friend "
    "today! Key Features: Made with high quality meat, vegetables and fruits Promotes health of "
    "skin, coat, urinary tract, immune system, eyes and heart Fortified with additional nutrients",

    ""
]
df_test['categories'] = ['Pet Supplies >  Cat Food',
                         'Health & Wellness > Medicine/Homeopathic Remedies']


class TestTextToNGrams():
    def test_string_default(self):
        a_ngrams = text_lists.text_to_ngrams("Testing-this_out")
        assert a_ngrams == [('testing',)]

    def test_string_all(self):
        a_all_ngrams = text_lists.text_to_ngrams("Testing this out", stop_words=None)
        assert a_all_ngrams == [('testing', 'this', 'out'), ('testing', 'this'),
                                ('this', 'out'), ('testing',), ('this',), ('out',)]

    def test_string_punctuation(self):
        a_punct_ngrams = text_lists.text_to_ngrams("Testing-this_out", remove_punctuation=False)
        assert a_punct_ngrams == [('testing-this_out',)]

    def test_list(self):
        a_ngrams = text_lists.text_to_ngrams(["testing", "this", "out"])
        assert a_ngrams == [('testing', 'this', 'out'), ('testing', 'this'),
                            ('this', 'out'), ('testing',), ('this',), ('out',)]


class TestTextListsToNGrams():
    def test_string_default(self):
        a_ngrams = text_lists.text_list_to_ngram_list(["Testing-this_out"])
        assert a_ngrams == [[('testing',)]]

    def test_string_all(self):
        a_all_ngrams = text_lists.text_list_to_ngram_list(["Testing this out"], stop_words=None)
        assert a_all_ngrams == [[('testing', 'this', 'out'), ('testing', 'this'),
                                 ('this', 'out'), ('testing',), ('this',), ('out',)]]

    def test_string_punctuation(self):
        a_punct_ngrams = text_lists.text_list_to_ngram_list(["Testing-this_out"],
                                                            remove_punctuation=False)
        assert a_punct_ngrams == [[('testing-this_out',)]]

    def test_list(self):
        a_ngrams = text_lists.text_list_to_ngram_list([["testing", "this", "out"]])
        assert a_ngrams == [[('testing', 'this', 'out'), ('testing', 'this'),
                             ('this', 'out'), ('testing',), ('this',), ('out',)]]


class TestSentenceTokenize():
    def test_description_sentences(self):
        a_test_description = text_lists.sentence_tokenize([df_test['descriptions'][0]])
        a_result = [[
            ('Wellness Cat Food is specially formulated with high quality protein and'
             ' natural sources of essential nutrients that your cat needs.'),
            ('This formula contains premium quality meat, sweet potatoes and carrots '
             'and fresh berries to offer excellent sources of protein, vitamins, minerals,'
             ' fatty acids, and amino acids.'),
            ('These nutrients are carefully chosen to ensure your cat\'s skin, coat, '
             'urinary tract, immune system, eyes and heart are properly maintained.'),
            ('This can of Wellness Cat Food comes with 5.5 ounces of a palatable chicken '
             'and lobster formula.'),
            'There is also a Healthy Indulgence formula available for cats with grain allergies.',
            ('Your cat will love this scrumptious food, so order a can for your feline friend'
             ' today!'),
            ('Key Features: Made with high quality meat, vegetables and fruits Promotes health of'
             ' skin, coat, urinary tract, immune system, eyes and heart Fortified with additional'
             ' nutrients')
        ]]
        assert a_test_description == a_result


class TestDatasetNGrams():
    t_ngrams = text_lists.dataset_ngrams(a_titles=df_test['titles'],
                                         a_descriptions=df_test['descriptions'],
                                         a_brands=df_test['brands'],
                                         a_categories=df_test['categories'],
                                         b_return_percents=False,
                                         i_max_size=2)
    e_df_ngrams, e_pn_ngrams, e_desc_ngrams, e_brand_ngrams, e_mc_ngrams, t_counts = t_ngrams

    def test_counts(self):
        assert self.t_counts == (169, 18, 139, 2, 10)

    def test_pn_ngrams(self):
        e_test_pn_ngrams = {
            ('0.0_oz',): 1, ('0.0_oz', 'case'): 1, ('00',): 1, ('0_oz',): 1,
            ('canned',): 1, ('canned', 'cat'): 1, ('case',): 1, ('case', 'of'): 1,
            ('cat',): 1, ('cat', 'food'): 1, ('chicken',): 1, ('chicken', 'with'): 1,
            ('entree',): 1, ('entree', 'canned'): 1, ('food',): 1, ('food', '0.0_oz'): 1,
            ('liver',): 1, ('liver', 'entree'): 1, ('nasarinse',): 1,
            ('nasarinse', 'naso-gel'): 1, ('naso-gel',): 1, ('naso-gel', '0_oz'): 1,
            ('neilmed',): 1, ('neilmed', 'nasarinse'): 1, ('of',): 1, ('of', '00'): 1,
            ('selects',): 1, ('selects', 'chicken'): 1, ('signature',): 1,
            ('signature', 'selects'): 1, ('wellness',): 1, ('wellness', 'signature'): 1,
            ('with',): 1, ('with', 'liver'): 1
        }
        assert self.e_pn_ngrams == e_test_pn_ngrams

    def test_desc_ngrams(self):
        e_test_desc_ngrams = {
            ('0.0',): 1, ('ounces',): 1, ('0.0', 'ounces',): 1, ('ounces', 'of'): 1, ('a',): 3, ('a', 'can'): 1,
            ('a', 'healthy'): 1, ('a', 'palatable'): 1, ('acids',): 1, ('acids', 'and'): 1,
            ('additional',): 1, ('additional', 'nutrients'): 1, ('allergies',): 1, ('also',): 1,
            ('also', 'a'): 1, ('amino',): 1, ('amino', 'acids'): 1, ('and',): 5, ('and', 'amino'): 1,
            ('and', 'carrots'): 1, ('and', 'fresh'): 1, ('and', 'fruits'): 1, ('and', 'heart'): 2,
            ('and', 'lobster'): 1, ('and', 'natural'): 1, ('are',): 1, ('are', 'carefully'): 1,
            ('are', 'properly'): 1, ('available',): 1, ('available', 'for'): 1, ('berries',): 1,
            ('berries', 'to'): 1, ('can',): 2, ('can', 'for'): 1, ('can', 'of'): 1, ('carefully',): 1,
            ('carefully', 'chosen'): 1, ('carrots',): 1, ('carrots', 'and'): 1, ('cat',): 3,
            ('cat', 'food'): 2, ('cat', 'needs'): 1, ('cat', 'will'): 1, ("cat's",): 1,
            ("cat's", 'skin'): 1, ('cats',): 1, ('cats', 'with'): 1, ('chicken',): 1,
            ('chicken', 'and'): 1, ('chosen',): 1, ('chosen', 'to'): 1, ('coat',): 2,
            ('coat', 'urinary'): 2, ('comes',): 1, ('comes', 'with'): 1, ('contains',): 1,
            ('contains', 'premium'): 1, ('ensure',): 1, ('ensure', 'your'): 1, ('essential',): 1,
            ('essential', 'nutrients'): 1, ('excellent',): 1, ('excellent', 'sources'): 1, ('eyes',): 2,
            ('eyes', 'and'): 2, ('fatty',): 1, ('fatty', 'acids'): 1, ('features',): 1,
            ('features', 'made'): 1, ('feline',): 1, ('feline', 'friend'): 1, ('food',): 3,
            ('food', 'comes'): 1, ('food', 'is'): 1, ('food', 'so'): 1, ('for',): 2, ('for', 'cats'): 1,
            ('for', 'your'): 1, ('formula',): 3, ('formula', 'available'): 1, ('formula', 'contains'): 1,
            ('formulated',): 1, ('formulated', 'with'): 1, ('fortified',): 1, ('fortified', 'with'): 1,
            ('fresh',): 1, ('fresh', 'berries'): 1, ('friend',): 1, ('friend', 'today'): 1, ('fruits',): 1,
            ('fruits', 'promotes'): 1, ('grain',): 1, ('grain', 'allergies'): 1, ('health',): 1,
            ('health', 'of'): 1, ('healthy',): 1, ('healthy', 'indulgence'): 1, ('heart',): 2,
            ('heart', 'are'): 1, ('heart', 'fortified'): 1, ('high',): 2, ('high', 'quality'): 2,
            ('immune',): 2, ('immune', 'system'): 2, ('indulgence',): 1, ('indulgence', 'formula'): 1,
            ('is',): 2, ('is', 'also'): 1, ('is', 'specially'): 1, ('key',): 1, ('key', 'features'): 1,
            ('lobster',): 1, ('lobster', 'formula'): 1, ('love',): 1, ('love', 'this'): 1, ('made',): 1,
            ('made', 'with'): 1, ('maintained',): 1, ('meat',): 2, ('meat', 'sweet'): 1,
            ('meat', 'vegetables'): 1, ('minerals',): 1, ('minerals', 'fatty'): 1, ('natural',): 1,
            ('natural', 'sources'): 1, ('needs',): 1, ('nutrients',): 3, ('nutrients', 'are'): 1,
            ('nutrients', 'that'): 1, ('of',): 4, ('of', 'a'): 1, ('of', 'essential'): 1,
            ('of', 'protein'): 1, ('of', 'skin'): 1, ('of', 'wellness'): 1, ('offer',): 1,
            ('offer', 'excellent'): 1, ('order',): 1, ('order', 'a'): 1, ('palatable',): 1,
            ('palatable', 'chicken'): 1, ('potatoes',): 1, ('potatoes', 'and'): 1, ('premium',): 1,
            ('premium', 'quality'): 1, ('promotes',): 1, ('promotes', 'health'): 1, ('properly',): 1,
            ('properly', 'maintained'): 1, ('protein',): 2, ('protein', 'and'): 1, ('protein', 'vitamins'): 1,
            ('quality',): 3, ('quality', 'meat'): 2, ('quality', 'protein'): 1, ('scrumptious',): 1,
            ('scrumptious', 'food'): 1, ('skin',): 2, ('skin', 'coat'): 2, ('so',): 1, ('so', 'order'): 1,
            ('sources',): 2, ('sources', 'of'): 2, ('specially',): 1, ('specially', 'formulated'): 1,
            ('sweet',): 1, ('sweet', 'potatoes'): 1, ('system',): 2, ('system', 'eyes'): 2,
            ('that',): 1, ('that', 'your'): 1, ('there',): 1, ('there', 'is'): 1, ('these',): 1,
            ('these', 'nutrients'): 1, ('this',): 3, ('this', 'can'): 1, ('this', 'formula'): 1,
            ('this', 'scrumptious'): 1, ('to',): 2, ('to', 'ensure'): 1, ('to', 'offer'): 1, ('today',): 1,
            ('tract',): 2, ('tract', 'immune'): 2, ('urinary',): 2, ('urinary', 'tract'): 2, ('vegetables',): 1,
            ('vegetables', 'and'): 1, ('vitamins',): 1, ('vitamins', 'minerals'): 1, ('wellness',): 2,
            ('wellness', 'cat'): 2, ('will',): 1, ('will', 'love'): 1, ('with',): 4, ('with', '0.0'): 1,
            ('with', 'additional'): 1, ('with', 'grain'): 1, ('with', 'high'): 2, ('your',): 3,
            ('your', 'cat'): 2, ('your', "cat's"): 1, ('your', 'feline'): 1
        }
        assert self.e_desc_ngrams == e_test_desc_ngrams

    def test_brand_ngrams(self):
        e_test_brand_ngrams = {('neilmed',): 1, ('wellness',): 1}
        assert self.e_brand_ngrams == e_test_brand_ngrams

    def test_cat_ngrams(self):
        e_test_cat_ngrams = {('cat', 'food'): 1,
                             ('food',): 1,
                             ('health',): 1,
                             ('health', '&', 'wellness'): 1,
                             ('homeopathic', 'remedies'): 1,
                             ('medicine', 'homeopathic', 'remedies'): 1,
                             ('medicine/homeopathic', 'remedies'): 1,
                             ('pet', 'supplies'): 1,
                             ('remedies',): 1,
                             ('supplies',): 1,
                             ('wellness',): 1}
        assert self.e_mc_ngrams == e_test_cat_ngrams


class Doc(list):
    ''' Spacy Doc standin for unit test '''
    __name__ = 'Doc'
    def __repr__(self):
        return ''.join(self)

class Token(str):
    ''' Spacy Token standin for unit test '''
    __name__ = 'Token'
    def __init__(self, content):
        self.string = content
        self.text = content.strip()
    
    def __repr__(self):
        return self.text


class TestDescText():
    #NLP = spacy.load('en')
    #nlp_test = NLP('This is a 1.0 Test')
    nlp_test = Doc(Token(word) for word in ['This ', 'is ', 'a ', '1.0 ', 'Test'])
    
    def test_standard(self):
        a_test = text_lists.desc_text(self.nlp_test)
        assert a_test == ['This', 'is', 'a', '1.0', 'Test']

    def test_normalize(self):
        a_test = text_lists.desc_text(self.nlp_test, b_normalize=True)
        assert a_test == ['This', 'is', 'a', '0.0', 'Test']

    def test_lower(self):
        a_test = text_lists.desc_text(self.nlp_test, b_lower=True)
        assert a_test == ['this', 'is', 'a', '1.0', 'test']
