import os
import setuptools
from setuptools.command.install import install
import re
import sys

module_path = os.path.join(os.path.dirname(__file__), 'FwMP/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_dependencies()
        #download_spacy_model()
        install.run(self)

    def install_fw_dependencies(self):
        print("About to install FindWatt dependencies...")
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip install git+https://bitbucket.org/FindWatt/fwutility")
            os.system("pip install git+https://bitbucket.org/FwPyClassification/fwptypes")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwutility")
            os.system("pip3 install git+https://bitbucket.org/FwPyClassification/fwptypes")

#def download_spacy_model():
#    # try to check if spacy model is already downloaded and installed
#    import spacy
#    try:
#        o_nlp = spacy.load('en')
#        if o_nlp.__dict__.get('path'): return None
#    except:
#        pass
#        
#    print("About to download spacy model...")
#    os.system("python -m spacy download en")

setuptools.setup(
    name="FwMP",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwmp",

    author="FindWatt",

    description="MP Functions for processing large amounts of data",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    # package_data={'': ["training_data/*.xlsx"]},
    package_data={'': []},
    py_modules=['FwMP'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "pandas",
        "numpy",
#        "spacy",
    ],
    cmdclass={'install': CustomInstall}
)
